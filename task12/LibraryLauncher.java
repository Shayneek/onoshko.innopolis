package ru.Onoshko.task12;

import java.io.*;

public class LibraryLauncher {
    Library library = new Library();


    private static final String library1 = "Библиотека.bin";


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Library library = new Library();
        Book book = new Book("Бла-бла-бла", "Идиот", 2019);
        library.addBooks(book);
        Book book2 = new Book("Java", "Pit", 1997);
        library.addBooks(book2);
        //library.listBooks();
        save(library);
        Library library2 = load();
        System.out.println(library2);
    }

    private static void save(Library library) {
        try (OutputStream os = new FileOutputStream(library1);
            ObjectOutputStream fout = new ObjectOutputStream(os)) {
            fout.writeObject(library);
        } catch (IOException e) {
        }
    }


    private static Library load() throws IOException, ClassNotFoundException {
        try (InputStream is = new FileInputStream(library1);
             ObjectInputStream ois = new ObjectInputStream(is)) {
            return (Library) ois.readObject();

        }
    }
}

