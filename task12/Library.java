package ru.Onoshko.task12;

import java.io.*;
import java.util.Arrays;

public class Library implements Serializable{
    private Book[] books = new Book[0];

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    public void addBooks (Book book){
        Book [] newBooks = new Book[books.length+1];
        System.arraycopy(books,0,newBooks,0,books.length);
        newBooks[books.length] = book;
        books = newBooks;
    }

    @Override
    public String toString() {
        return "Library{" +
                "books=" + Arrays.toString(books) +
                '}';
    }

    public void listBooks() {
        for (Book book : books){
            System.out.println(book);
        }
    }
}


