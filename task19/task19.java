package ru.Onoshko.task19;

import java.util.HashSet;
import java.util.Set;

public class task19 {


    public static void main(String[] args) {
        task19 t1 = new task19();


        Set<String> set = new HashSet<>();

        set.add("foo");
        set.add("buzz");
        set.add("bar");
        set.add("fork");


        System.out.println(t1.removeEvenLength(set));
    }

    public Set<String> removeEvenLength(Set<String> set) {
        Set<String> get = new HashSet<>();
        Object[] myarr = set.toArray();
        for (int i = 0; i < myarr.length; i++) {
            String s = (String) myarr[i];
            int d = s.length() % 2;
            if (d != 0) {
                get.add(s);
            }
        }
        return get;


    }
}

