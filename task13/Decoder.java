package ru.Onoshko.task13;

import java.io.*;
import java.nio.charset.Charset;


public class Decoder {

    public static void main(String[] args) throws IOException {
        File file = new File("utf8.txt");
        try (OutputStream os = new FileOutputStream(file)) {
            String s = "Привет, Мир!";
            os.write(s.getBytes());
        }
        try (InputStream is = new FileInputStream("utf8.txt")) {
            byte[] buf = new byte[100];
            if (is.read(buf) != -1) {
                String s = new String(buf);
                try (OutputStream os = new FileOutputStream("koi8.txt")){
                    os.write(s.getBytes(Charset.forName("KOI8")));
                }

            }
        }
    }

}
