package ru.Onoshko.task6.Calculator;

public class Calculator {

    public static void sum(int a, int b) {
        int sumRes = a + b;
        System.out.println("Сумма: " + sumRes);
    }

    public static void sum(double a, double b) {
        double sumRes = a + b;
        System.out.println("Сумма: " + sumRes);
    }

    public static void del(int a, int b) {
        int delRes = a / b;
        System.out.println("Частное: " + delRes);
    }

    public static void del(double a, double b) {
        double delRes = a / b;
        System.out.println("Частное: " + delRes);
    }

    public static void raz(int a, int b) {
        int razRes = a - b;
        System.out.println("Разность: " + razRes);
    }

    public static void raz(double a, double b) {
        double razRes = a - b;
        System.out.println("Разность: " + razRes);
    }

    public static void um(int a, int b) {
        int umRes = a * b;
        System.out.println("Частное: " + umRes);
    }

    public static void um(double a, double b) {
        double umRes = a * b;
        System.out.println("Частное: " + umRes);
    }

    public static void proc(int a, int b) {
        int procRes = a / 100 * b;
        System.out.println("Процент: " + procRes);
    }

    public static void proc(double a, double b) {
        double procRes = a / 100 * b;
        System.out.println("Частное: " + procRes);
    }

}
