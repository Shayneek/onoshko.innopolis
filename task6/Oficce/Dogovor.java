package ru.Onoshko.task6.Oficce;

import java.util.Arrays;

public class Dogovor {
    private int numver;
    private String date;
    private String[] title;

    public Dogovor(int numver, String date, String[] title) {
        this.numver = numver;
        this.date = date;
        this.title = title;
    }

    public int getNumver() {
        return numver;
    }

    public void setNumver(int numver) {
        this.numver = numver;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String[] getTitle() {
        return title;
    }

    public void setTitle(String[] title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Dogovor{" +
                "numver=" + numver +
                ", date='" + date + '\'' +
                ", title=" + Arrays.toString(title) +
                '}';
    }
}
