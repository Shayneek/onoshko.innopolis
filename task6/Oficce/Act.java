package ru.Onoshko.task6.Oficce;

import java.util.Arrays;

public class Act {
    private int numver;
    private String date;
    private String[] title;

    public Act(int numver, String date, String[] title) {
        this.numver = numver;
        this.date = date;
        this.title = title;
    }

    @Override
    public String toString() {
        return "Акт{" +
                "№=" + numver +
                ", дата='" + date + '\'' +
                ", список товара=" + Arrays.toString(title) +
                '}';
    }
}
