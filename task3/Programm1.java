package ru.Onoshko.task3;

import java.util.Scanner;

//поиск наименьшего из двух чисел
public class Programm1 {
    public static void main(String[] args) {
        int a, b, c;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первое число");
        a = scanner.nextInt();
        System.out.println("Введите второе число");
        b = scanner.nextInt();
        c = (a > b) ? b : a;
        System.out.println("Меньшее число");
        System.out.print(c);
    }
}

