package ru.Onoshko.task3;

// прогрессия

import javafx.beans.property.SimpleObjectProperty;

import java.util.Scanner;

public class Programm4 {
    private static String str, res;
    private static int b, q, a, d;

    public static void main(String[] args) {
        System.out.println("Введите желаемую прогрессии (арифметическая или геометрическая)");
        Scanner scanner = new Scanner(System.in);
        str = scanner.nextLine();
        res = str.toLowerCase();
        res = res.trim();
        switch (res) {
            case "геометрическая":
                System.out.println("Введите первый член геометрической прогрессии");
                b = scanner.nextInt();
                System.out.println("Введите знаменатель прогрессии");
                q = scanner.nextInt();
                System.out.println("Ваша прогрессия: ");
                int[] geo = new int[5];
                for (int i = 0; i < geo.length; i++) {
                    geo[i] = (int) (b * Math.pow(q, i));
                    System.out.printf("%4d", geo[i]);
                }
                break;
            case "арифметическая":
                System.out.println("Введите первый член арифметической  прогрессии");
                a = scanner.nextInt();
                System.out.println("Введите разность арифметической прогрессии");
                d = scanner.nextInt();
                System.out.println("Ваша прогрессия: ");
                int[] ari = new int[10];
                for (int i = 0; i < ari.length; i++) {
                    ari[i] = a + d * i;
                    System.out.printf("%4d", ari[i]);
                }
                break;
        }
    }
}
