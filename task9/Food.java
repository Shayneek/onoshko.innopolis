package ru.Onoshko.task9;

public enum Food {
    PORRIDGE(1,"Каша"),
    APLLE(2,"Яблоко"),
    CARROT(3, "Морковка"),
    MILK(4, "Молоко");

    private int number;
    private String titleFood;

    static Food[] foods = Food.values();

    Food(int number, String titleFood) {
        this.number = number;
        this.titleFood = titleFood;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitleFood() {
        return titleFood;
    }

    public void setTitleFood(String titleFood) {
        this.titleFood = titleFood;
    }


    @Override
    public String toString() {
        return "Еда{" +
                "№" + number +
                ", Название блюда ='" + titleFood + '\'' +
                '}';
    }
}
