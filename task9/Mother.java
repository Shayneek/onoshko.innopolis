package ru.Onoshko.task9;

import ru.Onoshko.task5.Drink;

public class Mother {


    public static void main(String[] args) {
        Child child = new Child();
        child.showMeny();
        try {
            child.eat();
        } catch (FoodExeption foodExeption) {
            foodExeption.printStackTrace();
        } finally {
            System.out.println("Спасибо мама");
        }
    }
}