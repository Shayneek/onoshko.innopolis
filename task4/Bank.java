package ru.Onoshko.task4;

public class Bank {
    int i;
    static CreditCard creditCards[] = new CreditCard[4];
    static DebitCard debitCards[] = new DebitCard[4];

    void creatureCreditCard() {
        Person cardHolder = new Person("Александр", "Оношко", "11.10.1990");
        creditCards[0] = new CreditCard(cardHolder, 500);
    }

    void creatureDebitCard() {
        Person cardHolder = new Person("Александр", "Оношко", "11.10.1990");
        debitCards[0] = new DebitCard(cardHolder);
    }

    // показать все карты
    void showAllCard() {
        i = 0;
        while (i < creditCards.length) {
            System.out.println(creditCards[i]);
            while (i < debitCards.length) {
                System.out.println(debitCards[i]);
                i = i + 1;
            }
        }
    }

    //Обслуживание всех карт
    void serviceAllCard() {
        i = 0;
        while (i < creditCards.length) {
            creditCards[i].serviceCard();
            while (i < debitCards.length) {
                debitCards[i].serviseCard();
                i = i + 1;

            }
        }
    }
}
