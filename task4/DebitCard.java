package ru.Onoshko.task4;

public class DebitCard extends Card {

    // списание средст за обслуживание
    void serviseCard() {
        balance = (balance - 5);
    }

    public DebitCard(Person cardHolder) {
        super(cardHolder);
    }
}
