package ru.Onoshko.task4;


public class Card {

    String number;
    int balance;
    Person cardHolder;

    public Card() {
    }

    public Card(Person cardHolder) {
        this.cardHolder = cardHolder;
    }

    public Card(String number, int balance, Person cardHolder) {
        this.number = number;
        this.balance = balance;
        this.cardHolder = cardHolder;
    }

    // снять деньги
    void getMoney(int money) {
        balance = balance - money;
    }

    // внести деньги
    void takeMoney(int money){
        balance = balance + money;
    }

    // проверка баланса
    void showMoney(){
        System.out.println("Ваш баланс: " + balance);
    }

    // списание средст за обслуживание
    void serviseCard(){
        balance = (balance- balance/100);
    }

    @Override
    public String toString() {
        return "Card{" +
                "number='" + number + '\'' +
                ", balance=" + balance +
                ", cardHolder=" + cardHolder +
                '}';
    }
}
