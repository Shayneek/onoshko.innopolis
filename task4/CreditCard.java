package ru.Onoshko.task4;

public class CreditCard extends Card {
    int creditLimit;
    int creditLimit1;

    public CreditCard(String number, int balance, Person cardHolder, int creditLimit) {
        super(number, balance, cardHolder);
        this.creditLimit = creditLimit;
    }

    public CreditCard(Person cardHolder, int creditLimit) {
        super(cardHolder);
        this.creditLimit = creditLimit;
    }

    // выдача денег с учетом кредита
    void getMoney(int money){
        if (money >(balance+creditLimit)){
            System.out.println("Привышен кредитный лемит");
        }
        else {
            if (money > balance){
                balance = balance - money;
                creditLimit = creditLimit -(Math.abs(balance));
                creditLimit1 = creditLimit;
                balance = 0;
            }
            else {
                if (money <= balance){
                    balance = balance - money;
                }
            }
        }
    }
    // начисление процента на остаток по кредитному лимиту
    void serviceCard(){
        balance = balance - (creditLimit - creditLimit1)/100*30;
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "creditLimit=" + creditLimit +
                ", number='" + number + '\'' +
                ", balance=" + balance +
                ", cardHolder=" + cardHolder +
                '}';
    }
}
