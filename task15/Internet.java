package ru.Onoshko.task15;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.Authenticator;

import java.io.*;
//import java.net.MalformedURLException;
import java.net.URL;

public class Internet {

    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            URL url = new URL("http://api.icndb.com/jokes/random");
            try (InputStream is = url.openStream();
                 Reader reader = new InputStreamReader(is);
                 BufferedReader br = new BufferedReader(reader)) {
                Chuck chuck = objectMapper.readValue(br, Chuck.class);
                System.out.println(chuck);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
