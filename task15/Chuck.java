package ru.Onoshko.task15;

public class Chuck {
    String type;
    Joke value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Joke getValue() {
        return value;
    }

    public void setValue(Joke value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Chuck{" +
                "type='" + type + '\'' +
                ", value=" + value +
                '}';
    }
}
