package ru.Onoshko.task16;

public class ToLeft {
    public static int[][] arr = {{1, 2, 3, 4, 5,6,7}, {5, 4, 3, 2, 1}};

    public void toLeft(int one, int one1, int two) {
        arr[two][one] = arr[two][one1];
    }

    public void arrToLeft() {
        int out, in;
        for (out = 0; out < arr.length; out++)
            for (in = 0; in < arr[out].length - 1; in++)
                toLeft(in, in + 1, out);
    }

    public void arrToZero() {
        int out, in;
        for (out = 0; out < arr.length; out++)
            arr[out][arr[out].length - 1] = 0;
    }

    public void Displey() {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        ToLeft arv = new ToLeft();
        arv.arrToLeft();
        arv.arrToZero();
        arv.Displey();


    }
}
