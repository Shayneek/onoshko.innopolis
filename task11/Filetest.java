package ru.Onoshko.task11;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Filetest {

    private static final Logger logger = Logger.getLogger(Filetest.class);

    public static void main(String[] args) throws IOException {

        File file = new File("C:/Users/User/IdeaProjects/ru.Onoshko/task11.txt");
        try {
            logger.log(Level.INFO, "Создание файла");
            file.createNewFile();
            } catch (IOException e) {
            logger.log(Level.ERROR, "не удалось создать файд" + e.getMessage());
        }
        File dir = new File("task11.1");
        logger.log(Level.INFO, "Создание каталога");
        dir.mkdir();

        logger.log(Level.INFO, "переименование файла:  "+ file.getName());
        file.renameTo(new File("task11.2.txt"));

        logger.log(Level.INFO, "Копирование файла:  "+ file.getName());


        File original=new File("C:/Users/User/IdeaProjects/ru.Onoshko/task12.txt");
        Path originalPath = original.toPath();
        Path copied = Paths.get("C:/Users/User/IdeaProjects/ru.Onoshko/task12.txt");
        Files.copy(originalPath,copied);



        logger.log(Level.INFO, "удаление файла:  " + file.getName());
        file.delete();
        logger.log(Level.INFO, "удаление каталога:  " + dir.getName());
        dir.delete();
    }
}
