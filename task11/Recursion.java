package ru.Onoshko.task11;

import java.io.File;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Recursion {

    public static void main(String[] args) {
        File dir = new File("Dir");

        transfer(dir);
    }

    private static void transfer(File dir) {
        System.out.println(dir.getName());
        //System.out.print(" ");
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()){
                System.out.print(" ");
                System.out.println(file.getName());
            }else {
                System.out.print(" ");
                transfer(file);
            }


            }

        }

    }

