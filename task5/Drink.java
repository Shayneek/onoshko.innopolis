package ru.Onoshko.task5;

public enum Drink {
    FANTA(1,100,"Фанта"),
    VODKA(2,500,"Водка"),
    BEER(3,150,"Пиво");

    private int number;
    private int costDrink;
    private String titleDrink;

    Drink(int number, int costDrink, String titleDrink) {
        this.number = number;
        this.costDrink = costDrink;
        this.titleDrink = titleDrink;
    }

    public int getCostDrink() {
        return costDrink;
    }

    public void setCostDrink(int costDrink) {
        this.costDrink = costDrink;
    }

    public String getTitleDrink() {
        return titleDrink;
    }

    public void setTitleDrink(String titleDrink) {
        this.titleDrink = titleDrink;
    }

    public int getNumber() {
        return number;
    }

    public void setNimber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Меню{" +
                "№| = " + number +
                ", цена| = " + costDrink +
                ", наименование| = '" + titleDrink + '\'' +
                '}';
    }
}




