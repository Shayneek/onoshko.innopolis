package ru.Onoshko.task5;

import java.util.Scanner;

public class VendingMachine {

    private int i;
    private int cash;
    private int balance;


    static Drink[] drinks = Drink.values();

    //Вносим деньги
    void takeCash() {
        balance = 0;
        System.out.println("Внесите деньги");
        Scanner scanner = new Scanner(System.in);
        cash = scanner.nextInt();
        System.out.println("Вы внесли: " + cash);
        balance = balance + cash;
    }

    // Вывод меню
    void showmeny() {
        for (Drink drink : drinks)
            System.out.println(drink);
    }


    // Выбор напитка
    void choiceDrink() {
        i = 4;
        while (i >= drinks.length) {
            System.out.println("Выберите напиток");
            Scanner scanner = new Scanner(System.in);
            i = scanner.nextInt() - 1;
            if (i >= drinks.length) {
                System.out.println("Вы выбрали несуществующий номер напитка: ");
            } else {
                System.out.println("Вы выбрали : " + drinks[i]);
            }
        }
    }

    // проверка и выдача напитка
    void takeDrink() {
        while (balance<drinks[i].getCostDrink())
        if (balance >= drinks[i].getCostDrink()) {
            System.out.println("Спасибо за покупку");
        } else {
            System.out.println("вы внесли недостаточно средст");
            System.out.println("Внесите деньги");
            Scanner scanner = new Scanner(System.in);
            cash = scanner.nextInt();
            System.out.println("Вы внесли: " + cash);
            balance = balance + cash;

        }
        System.out.println("Спасибо за покупку");
    }


    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "VendingMachine{" +
                "cash=" + cash +
                ", balance=" + balance +
                '}';
    }
}
