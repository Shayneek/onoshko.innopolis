package ru.Onoshko.task5;

import java.util.Scanner;

public class Money {

    private int cash;
    private int balance;


    //Вносим деньги
    void takeCash(){
        System.out.println("Внесите деньги");
        Scanner scanner = new Scanner(System.in);
        cash = scanner.nextInt();
        System.out.println("Вы внесли: " + cash);
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Money{" +
                "cash=" + cash +
                ", balance=" + balance +
                '}';
    }
}
