package ru.Onoshko.task10;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VendingMachine {

    private int i;
    private int cash;
    private int balance;


    static Drink[] drinks = Drink.values();

    //private static final Logger logger = Logger.getLogger(VendingMachine.class.getName());

    //Вносим деньги
    void takeCash() throws BillJamExeption {
        balance = 0;
        System.out.println("Внесите деньги");
        Scanner scanner = new Scanner(System.in);
        cash = scanner.nextInt();
        if (Math.random() < 0.2){
            throw new BillJamExeption(cash);
        }
        System.out.println("Вы внесли: " + cash);
        balance = balance + cash;
        System.out.println("Ваш баланс: " + balance);
    }

    // Вывод меню
    void showmeny() {
        for (Drink drink : drinks)
            System.out.println(drink);
    }


    // Выбор напитка
    void choiceDrink() {
        i = 4;
        while (i >= drinks.length) {
            System.out.println("Выберите напиток");
            Scanner scanner = new Scanner(System.in);
            i = scanner.nextInt() - 1;
            if (i >= drinks.length) {
                System.out.println("Вы выбрали несуществующий номер напитка: ");
            } else {
                System.out.println("Вы выбрали : " + drinks[i]);
            }
        }
    }

    // проверка и выдача напитка
    void takeDrink() throws BillJamExeption {
        while (balance<drinks[i].getCostDrink())
        if (balance >= drinks[i].getCostDrink()) {
            System.out.println("Спасибо за покупку");
        } else {
            System.out.println("вы внесли недостаточно средст");
            System.out.println("Внесите деньги");
            Scanner scanner = new Scanner(System.in);
            cash = scanner.nextInt();
            if (Math.random() < 0.2){
                throw new BillJamExeption(cash);
            }
            System.out.println("Вы внесли: " + cash);
            balance = balance + cash;
            System.out.println("Ваш баланс: " + balance);

        }
        System.out.println("Спасибо за покупку");
    }


    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "VendingMachine{" +
                "cash=" + cash +
                ", balance=" + balance +
                '}';
    }
}
