package ru.Onoshko.task10;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;


public class Launcher {
    private static final Logger logger = Logger.getLogger(Launcher.class);

    public static void main(String[] args) throws BillJamExeption {
        VendingMachine vm1 = new VendingMachine();
        logger.log(Level.INFO, "показ меню");
        vm1.showmeny();
        logger.log(Level.INFO, "Выбор напитка");
        vm1.choiceDrink();
        logger.log(Level.INFO, "Внесение наличных");
        try {
            vm1.takeCash();
        } catch (BillJamExeption e) {
            logger.log(Level.ERROR, e.getMessage(), e);
        }
        logger.log(Level.INFO, "Проверка баланса и выдача напитка");
        try {
            vm1.takeDrink();
        } catch (BillJamExeption e) {
            logger.log(Level.ERROR, e.getMessage(), e);

        }

    }
}
