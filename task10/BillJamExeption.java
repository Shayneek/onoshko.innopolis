package ru.Onoshko.task10;

public class BillJamExeption extends Exception {
    private final int cash;

    public BillJamExeption (int cash) {
        this.cash = cash;
    }

    @Override
    public String getMessage() {
        return "Произошло замятие купюры";
    }

    public int getCash() {
        return cash;
    }
}
