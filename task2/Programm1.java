package ru.Onoshko.task2;

import java.util.Scanner;

public class Programm1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите цену за литр:");
        double a = scanner.nextDouble(); // цена за литр бензина
        System.out.println("Введите количество литров:");
        int b = scanner.nextInt(); //количество литров бензина
        double c = a * b; // итоговая стоимость
        System.out.println("Итого:");
        System.out.print(c + " руб.коп");
    }
}