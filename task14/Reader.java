package ru.Onoshko.task14;

import java.io.*;
import java.util.Scanner;

public class Reader {

    public static void main(String[] args) throws IOException {
        File file = new File("out.txt");
        try (PrintStream writer = new PrintStream(file)) {
            try (InputStream is = new FileInputStream("products.txt")) {
                Scanner scanner = new Scanner(is);
                float total = 0;
                writer.printf("Наименование        Цена     Кол-во    Стомость %n");
                writer.printf("================================================%n");
                while (scanner.hasNext()) {
                    String name = scanner.nextLine();
                    float quantity = Float.parseFloat(scanner.nextLine());
                    float price = Float.parseFloat(scanner.nextLine());
                    total += quantity * price;
                    writer.printf("%-15s     %-5.2f    %-6.2f   =   %-5.2f %n", name, quantity, price, quantity * price);
                }
                writer.printf("=================================================%n");
                writer.printf("Итого:                                   %-5.2f %n", total);
            }
        }

    }
}



