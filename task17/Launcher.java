package ru.Onoshko.task17;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;

public class Launcher {
    public static void main(String[] args) {
        Person p = new Person(10L, "Андрей");
        Person p1 = new Person(11L, "Андрей");
        Person p2 = new Person(10L, "Адександр");
        Person p3 = new Person(40L, "Николай");
        Person p4 = new Person(65L, "Алефтина");

        List<Person> list= new ArrayList<>();
        list.add(p);
        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);

        System.out.println("-------до сортировки--------");
        for (Person u : list) {
            System.out.println(u);
        }
        System.out.println("-------после сортировки-----");
        Collections.sort(list, new PersonComparator());
        for (Person u : list) {
            System.out.println(u);


        }
    }
}
