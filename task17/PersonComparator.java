package ru.Onoshko.task17;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {

    public int compare(Person a, Person b) {
        int result = b.getAge().compareTo(a.getAge());

        if (result == 0) {
            result = b.getName().compareTo(a.getName());
        }
        return result;
    }
}
